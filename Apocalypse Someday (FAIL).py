import sys
sys.dont_write_bytecode = True
import time
start_time = time.time()

class ApocalypseSomeday:

    numbers = [[0, 666, 0]]

    def join(self, num):
        return int(str(num[0]) + str(num[1]) + str(num[2]))

    def separate(self, num):
        s = str(num)
        start = ""
        while(s[0:3] != "666"):
            start += s[0]
            s = s[1:]
        mid = s[0:3]
        end = s[3:]
        if start == "":
            start = 0
        if end == "":
            end = 0
        return [int(start), int(mid), int(end)]

    def addMore(self, num):
        if [num[0]+1, 666, num[2]] not in self.numbers:
            self.numbers.append([num[0]+1, 666, num[2]])
        if [num[0], 666, num[2]+1] not in self.numbers:
            self.numbers.append([num[0], 666, num[2]+1])
        if [num[0]+1, 666, num[2]+1] not in self.numbers:
            self.numbers.append([num[0]+1, 666, num[2]+1])

    def getNth(self, n):
        unsorted = 0
        while len(self.numbers) < n + 10**(int(len(str(n)))):
            self.addMore(self.numbers[unsorted])
            unsorted += 1
        res = [self.join(x) for x in self.numbers]
        for x in res:
            if str(x)[-1] == '0':
                res.append(x/10)
        res.sort()
        self.numbers = [self.separate(r) for r in res]
        print res
        return res[n-1]

a = ApocalypseSomeday()
a.getNth(187)


print "Run time: %s seconds" % (time.time() - start_time)
