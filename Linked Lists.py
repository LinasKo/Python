import sys
sys.dont_write_bytecode = True
import time
start_time = time.time()

class node():
    def __init__(self, data, nextn):
        self.data = data
        self.next = nextn

    def find_length_static(self, node):
        length = 1
        nextn = self.next
        while nextn != None:
            length += 1
            nextn = nextn.next
        return length

    def find_length_dyn(self):
        if self.next == None:
            return 1
        else:
            return 1 + self.next.find_length_dyn()

    def printout(self):
        if self.next == None:
            return str(self.data)
        else:
            return str(self.data) + ' ' + self.next.printout()

    def reverse(self):
        node = self
        previous = None
        while node.next != None:
            next = node.next
            node.next = previous
            previous = node
            node = next
        node.next = previous


    def reverse_for(self):
        nodes = []
        node = self
        while True:
            nodes.append(node)
            if node.next == None:
                break
            else:
                node = node.next
        for n in xrange(1,len(nodes)):
            nodes[n].next = nodes[n-1]
        nodes[0].next = None

print "Run time: %s seconds" % (time.time() - start_time)

n5 = node(555, None)
n4 = node(444, n5)
n3 = node(333, n4)
n2 = node(222, n3)
n1 = node(111, n2)

print 'dyn:', n1.find_length_dyn()
print 'stt:', n1.find_length_static(n1)
print 'data:', n1.printout()
n1.reverse()
print 'data reversed:', n5.printout()
n5.reverse()
print 'data normal  :', n1.printout()
print

n1.reverse_for()
print 'data reversed:', n5.printout()
n5.reverse_for()
print 'data normal  :', n1.printout()
