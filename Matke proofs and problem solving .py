import sys
sys.dont_write_bytecode = True
import time
start_time = time.time()

def mix(n):
    cards = range(1,2*n+1)
    mixed = []
    for i in cards:
        if i % 2 == 1:
            mixed.append((i-1)/2 + 1)
        else:
            mixed.append(n+i/2)
    return mixed

# Number of cards:
const = 10

const /= 2
a = range(1,const*2+1)
m = mix(const)
print a
print m
print

for i in range(len(a)):
    print a[i], "->", m[i]  


print "Run time: %s seconds" % (time.time() - start_time)
