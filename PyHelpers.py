def is_prime(a):
    if a == 0 or a == 1:
        return False
    for i in xrange(2,int(a**0.5)+1):
        if a % i == 0:
            return False
    return True

def next_prime(a):
    x = a
    while True:
        a += 1
        if is_prime(a):
            return a
